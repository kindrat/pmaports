_flavor=postmarketos-allwinner
_config="config-$_flavor.$CARCH"
pkgname=linux-$_flavor
pkgver=5.7.0_git20200705
pkgrel=2
arch="aarch64"
pkgdesc="Kernel fork with Pine64 patches"
url="https://gitlab.com/pine64-org/linux/"
license="GPL-2.0-only"
makedepends="devicepkg-dev perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev openssl-dev file bison flex rsync xz"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-anbox"
_commit="c3e42484fd1be12ef573bc77f91626ccb7efd1ad"
source="$pkgname-$_commit.tar.gz::https://gitlab.com/pine64-org/linux/-/archive/$_commit/linux-$_commit.tar.gz
	config-$_flavor.aarch64
	disable-power-save.patch
	touch-dts.patch
	convergence.patch
	"

subpackages="$pkgname-dev"

_carch=$CARCH
case "$_carch" in
aarch64*) _carch="arm64" ;;
arm*) _carch="arm" ;;
ppc*) _carch="powerpc" ;;
s390*) _carch="s390" ;;
esac

builddir="$srcdir/linux-$_commit"

prepare() {
	default_prepare

	REPLACE_GCCH=0 \
		. downstreamkernel_prepare
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor" \
		CFLAGS_MODULE=-fno-pic
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make -j1 modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

dev() {
	provides="linux-headers"
	replaces="linux-headers"

	cd $builddir

	# https://github.com/torvalds/linux/blob/master/Documentation/kbuild/headers_install.rst
	make -j1 headers_install \
		ARCH="$_carch" \
		INSTALL_HDR_PATH="$subpkgdir"/usr
}

sha512sums="3655b68deffcdd2384f900354695098d85a646ef55b581406d734ef27511f3cc6bd5084b5524955ca3536aed38375f07f0500d465faeb162cd13fd220281a494  linux-postmarketos-allwinner-c3e42484fd1be12ef573bc77f91626ccb7efd1ad.tar.gz
3a77fb1493fa82760cecfb1a6c1496da03bec2b24feaef4f950f08e50b9fc0ef5b5f2c11b1bbef192fd0a1d7298e92032c1931ceb44e752eef33b0822514b04d  config-postmarketos-allwinner.aarch64
3c0d9d282a36a5f6a442b434839d77851f9b20185725cb73aee88e6e209c68fd3d71df8e2a36ffcdb605c47f86df5dbcda5d00353c75c23303861936196924e0  disable-power-save.patch
c6e1ff1c060f68a59fa57a7cfc573a500fc8d200f56193530f7c1967e4f70f17cb2c930496f6a6489a6a10de130a2e66f5cd328eb6c4ae936f4af348a7413c3b  touch-dts.patch
ace7821ed3f31681aaf8cc983df155f98c01343d908cec1d32fcfd28999bbd9727ecca173a4b31026c966c6bf762f60549c3b09f7d7f8de74900271a7ad78d1a  convergence.patch"
